package com.projekty.felus.projektkoncowy_felus;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class PickedCollagueActivity extends AppCompatActivity {

    private ArrayList<ImageData> list;
    private FrameLayout kolazuLayout;
    private ImageView ivAktywne;
   // int lightGreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picked_collague);
        getSupportActionBar().hide();

        kolazuLayout = (FrameLayout) findViewById(R.id.kolazuLayout);

     //   lightGreen = Color.parseColor("#519c3f");

        list = (ArrayList<ImageData>) getIntent().getSerializableExtra("list");

        for(int i = 0, length = list.size() ; i<length ; i++){
            Log.wtf("lista","rozmiar listy: " + list.get(i).toString());
        }

        for(int i = 0, length = list.size() ; i<length ; i++){
            ImageView iv = new ImageView(PickedCollagueActivity.this);
            iv.setX(list.get(i).getX());
            iv.setY(list.get(i).getY());
            iv.setLayoutParams(new LinearLayout.LayoutParams( (int)(list.get(i).getWidth()),(int)(list.get(i).getHeight()) ));
            iv.setBackgroundColor(Color.DKGRAY);
            setListeners(iv);
            iv.setImageResource(R.drawable.camera);
            iv.setScaleType(ImageView.ScaleType.CENTER);
            kolazuLayout.addView(iv);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 100:
                Uri imgData = data.getData();
                InputStream stream = null;
                try {
                    stream = getContentResolver().openInputStream(imgData);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap b = BitmapFactory.decodeStream(stream);
                ivAktywne.setImageBitmap(b);
                break;
            case 200:
                Bundle extras = data.getExtras();
                Bitmap bM = (Bitmap) extras.get("data");
                ivAktywne.setImageBitmap(bM);
        }
        ivAktywne.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    private void setListeners(final ImageView iv){
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("ku","rde");
                ivAktywne = iv;
            }
        });

        iv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.wtf("ku","rcze");
                ivAktywne = iv;
                AlertDialog.Builder alert = new AlertDialog.Builder(PickedCollagueActivity.this);
                alert.setTitle("Skąd pobrać zdjęcie?");
                final String[] opts = {"galerie","wbudowana kamera","własna kamera"};

                alert.setItems(opts, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // wyswietl opcje[which]);
                        switch(which){
                            case 0:
                                Intent intent = new Intent(Intent.ACTION_PICK);
                                intent.setType("image/*");
                                startActivityForResult(intent, 100); // 100 - stała wartośc która posłuży do identyfikacji tej akcji
                                break;
                            case 1:
                                Intent intentCam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                //jesli jest dostepna zewnetrzny aparat
                                if (intentCam.resolveActivity(getPackageManager()) != null) {
                                    startActivityForResult(intentCam, 200); // 200 - jw
                                }
                                break;

                            case 2:
                                break;
                        }
                    }
                });
                alert.show();
                return false;
            }
        });

    }
}
