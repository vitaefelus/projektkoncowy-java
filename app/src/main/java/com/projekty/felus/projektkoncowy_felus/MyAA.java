package com.projekty.felus.projektkoncowy_felus;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;


import java.io.File;

/**
 * Created by 4ib1 on 2016-09-29.
 */


public class MyAA extends ArrayAdapter {

    private String[] array;
    private Context _context;

    public MyAA(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        array = objects;
        _context = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //inflater - klasa konwertująca xml na kod javy
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.land_cell, null);
        //szukam kontrolki w layoucie

        TextView tv1 = (TextView) convertView.findViewById(R.id.tvLandCell);
        tv1.setText(array[position]);
        //
        ImageView iv1 = (ImageView) convertView.findViewById(R.id.ivLandCell2);
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MyTagGoesHere", "klik w kosz");

                File toDelete = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        "KarolinaFelus");
                toDelete = new File(toDelete, array[position]);
                Log.i("file",toDelete.getAbsolutePath());

                for(File f : toDelete.listFiles()){
                    f.delete();
                }
                toDelete.delete();
                ((AlbumsActivity)_context).restartView();
            }

        });
        return convertView;

    }


}


