package com.projekty.felus.projektkoncowy_felus;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;

/**
 * Created by 4ib1 on 2016-12-01.
 */
public class PreviewText extends View {

    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private float _x,_y;
    private int _color, _strokeColor;
    private String _text;

    public PreviewText(Context context, Typeface typeface, String text, int color, int strokeColor) {
        super(context);
        _text = text;
        _color = color;
        _strokeColor = strokeColor;
        paint.reset();            // czyszczenie
        paint.setAntiAlias(true);    // wygładzanie
        paint.setTextSize(100);        // wielkość fonta
        paint.setTypeface(typeface);  // czcionka
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);
        canvas.drawText(_text, 50, 200, paint);
    }
}
