package com.projekty.felus.projektkoncowy_felus;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.SensorManager;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
//import android.view.OrientationEventListener;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.hardware.Camera;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import android.content.pm.PackageManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CameraActivity extends AppCompatActivity {

    private Camera camera;
    private int cameraId = -1;
    private CameraPreview _cameraPreview;
    private FrameLayout _frameLayout;
    private ImageView cameraBottomPanelPhoto;
    private byte[] fdata;
    private File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

    private Camera.Parameters camParams;

    private FrameLayout cameraLayout;
    private LinearLayout topMenuPanel;
    private LinearLayout bottomMenuPanel;

    private boolean clicked = true;

    private Mini miniature;

    private ImageView whiteBalance;
    private ImageView resolutions;
    private ImageView colorEffects;
    private ImageView lightCompensation;

    private Display display;
    private OrientationEventListener orientationEventListener;
    int curr=-1;

    private ArrayList<Mini> miniatureList = new ArrayList<Mini>();
    private ArrayList<ImageView> imageButtonList = new ArrayList<ImageView>();
    private ArrayList<byte[]> takenPhoto = new ArrayList<byte[]>() ;

    String[] opts = {
            "Podgląd","usuń bieżące","usuń wszystkie","zapisz bieżące","zapisz wszystkie"
    };

    private List<String> colorEffectsList;
    private List<Camera.Size> resolutionsList;
    private List<String> whiteBalanceList;
    private int minExposureCompensation,
            maxExposureCompensation,
            currentExposureCompensation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        cameraLayout = (FrameLayout) findViewById(R.id.frameLayout1);
        topMenuPanel = (LinearLayout) findViewById(R.id.topMenuPanel);
        bottomMenuPanel = (LinearLayout) findViewById(R.id.bottomMenuPanel);

        display = getWindowManager().getDefaultDisplay();

        cameraLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(clicked){
                    ObjectAnimator animTop = ObjectAnimator.ofFloat(topMenuPanel, View.TRANSLATION_Y, -200);
                    animTop.setDuration(300); //ms
                    animTop.start();
                    ObjectAnimator animBottom = ObjectAnimator.ofFloat(bottomMenuPanel, View.TRANSLATION_Y, 200);
                    animBottom.setDuration(300); //ms
                    animBottom.start();
                    clicked = false;
                }
                else{
                    ObjectAnimator animTop = ObjectAnimator.ofFloat(topMenuPanel, View.TRANSLATION_Y, 0);
                    animTop.setDuration(300); //ms
                    animTop.start();
                    ObjectAnimator animBottom = ObjectAnimator.ofFloat(bottomMenuPanel, View.TRANSLATION_Y, 0);
                    animBottom.setDuration(300); //ms
                    animBottom.start();

                    clicked = true;
                }
            }

        });

        orientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int i) {
              //  Log.wtf("aaa",Integer.toString(topMenuPanel.getChildCount()));

                if(i<=45||i>315){
                    if(curr==-270){
                        for(int j=0;j<miniatureList.size();j++){
                            ObjectAnimator.ofFloat(miniatureList.get(j), View.ROTATION, 90, 0)
                                    .setDuration(100)
                                    .start();
                        }
                        for(int j=0;j<imageButtonList.size();j++){
                            ObjectAnimator.ofFloat(imageButtonList.get(j), View.ROTATION, 90, 0)
                                    .setDuration(100)
                                    .start();
                        }
                    }
                    curr=0;
                }
                else if(i>45&&i<=135){
                    curr=-90;
                }
                else if(i>135&&i<=225){
                    curr=-180;
                }
                else if(i>225&&i<=315){
                    if(curr==0){
                        for(int j=0;j<miniatureList.size();j++) {
                            ObjectAnimator.ofFloat(miniatureList.get(j), View.ROTATION, -360, -270)
                                    .setDuration(100)
                                    .start();
                        }
                        for(int j=0;j<imageButtonList.size();j++){
                            ObjectAnimator.ofFloat(imageButtonList.get(j), View.ROTATION, -360, -270)
                                    .setDuration(100)
                                    .start();
                        }
                    }
                    curr=-270;
                }
                for(int j=0;j<miniatureList.size();j++) {
                    ObjectAnimator.ofFloat(miniatureList.get(j), View.ROTATION, curr)
                            .setDuration(100)
                            .start();
                }
                for(int j=0;j<imageButtonList.size();j++){
                    ObjectAnimator.ofFloat(imageButtonList.get(j), View.ROTATION, curr)
                            .setDuration(100)
                            .start();
                }
            }
        };
    }


    private void initCamera() {
        boolean cam = getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

        if (!cam) {
            // uwaga - brak kamery
        } else {
            // wykorzystanie danych zwróconych przez kolejną funkcję getCameraId
            cameraId = getCameraId();
            // jest jakaś kamera!
            if (cameraId < 0) {
                // brak kamery z przodu!
            } else if (cameraId >= 0) {
                camera = Camera.open(cameraId);
            }



            colorEffects = (ImageView) findViewById(R.id.colorEffects);
            colorEffects.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Efekty kolorystyczne");
                    //nie może mieć setMessage!!!
                    final String[] colors = new String[colorEffectsList.size()];

                    for (int i = 0; i < colorEffectsList.size(); i++) {
                        colors[i] = colorEffectsList.get(i);
                    }

                    alert.setItems(colors, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // wyswietl opcje[which]);
                            camParams.setColorEffect(colors[which]);
                            camera.setParameters(camParams);
                        }
                    });
//
                    alert.show();
                }
            });

            lightCompensation = (ImageView) findViewById(R.id.lightCompensation);
            lightCompensation.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Ekspozycja");
                    int tmpLength = maxExposureCompensation - minExposureCompensation;
                    final String[] lComp = new String[tmpLength];

                    for (int i = 0; i < tmpLength; i++) {
                        lComp[i] = (minExposureCompensation + i) + "";
                    }

                    alert.setItems(lComp, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // wyswietl opcje[which]);
                            camParams.setExposureCompensation(Integer.parseInt(lComp[which]));
                            camera.setParameters(camParams);

                            //
                        }
                    });
//
                    alert.show();
                }
            });

            whiteBalance = (ImageView) findViewById(R.id.whiteBalance);
            whiteBalance.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Balans bieli");
                    //nie może mieć setMessage!!!
                    final String[] whiteB = new String[whiteBalanceList.size()];

                    for (int i = 0; i < whiteBalanceList.size(); i++) {
                        whiteB[i] = whiteBalanceList.get(i);
                    }

                    alert.setItems(whiteB, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // wyswietl opcje[which]);
                            camParams.setWhiteBalance(whiteB[which]);
                            camera.setParameters(camParams);

                            //
                        }
                    });
//
                    alert.show();
                }
            });

            resolutions = (ImageView) findViewById(R.id.resolutions);
            resolutions.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(CameraActivity.this);
                    alert.setTitle("Rozdzielczości");
                    //nie może mieć setMessage!!!
                    final String[] res = new String[resolutionsList.size()];

                    for (int i = 0; i < resolutionsList.size(); i++) {
                        res[i] = resolutionsList.get(i).width + "x" + resolutionsList.get(i).height;
                    }

                    alert.setItems(res, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // wyswietl opcje[which]);
                            camParams.setPictureSize(resolutionsList.get(which).width, resolutionsList.get(which).height);
                            camera.setParameters(camParams);
                        }
                    });
                    alert.show();
                }
            });
        }

        cameraBottomPanelPhoto = (ImageView) findViewById(R.id.cameraBottomPanelPhoto);
        cameraBottomPanelPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View cameraBottomPanelPhoto) {
                camera.takePicture(null, null, camPictureCallback);
            }
        });

        imageButtonList.add(colorEffects);
        imageButtonList.add(lightCompensation);
        imageButtonList.add(whiteBalance);
        imageButtonList.add(resolutions);
        imageButtonList.add(cameraBottomPanelPhoto);
    }

    private Camera.PictureCallback camPictureCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            // zapisz dane zdjęcia w tablicy typu byte[]
            // do poźniejszego wykorzystania
            // poniewaz zapis zdjęcia w galerii powinien być dopiero po akceptacji butonem
            fdata = data;

            takenPhoto.add(data);
           // Log.wtf("TAKEN PHOTO SIZE:",Integer.toString(takenPhoto.size()));

            final Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            final Bitmap smallBmp = Bitmap.createScaledBitmap(bitmap, 250, 250, false);

            Matrix matrix = new Matrix();
            matrix.postRotate(-90f);
            final Bitmap rotatedBitmap = Bitmap.createBitmap(smallBmp , 0, 0, smallBmp.getWidth(), smallBmp.getHeight(), matrix, true);

            final Mini miniature = new Mini(CameraActivity.this,250,250,rotatedBitmap);


            miniatureList.add(miniature);

            //usuwa z layoutu wszystkie dzieci oprócz kamery i koła
            for(int i=_frameLayout.getChildCount()-1;i>=2;i--){
                _frameLayout.removeViewAt(i);
            }

            int nr=miniatureList.size();
            for(int i=0;i<nr;i++){
                _frameLayout.addView(miniatureList.get(i));
                miniatureList.get(i).setX((float) (display.getWidth()/2-120+200* Math.cos(2*Math.PI/nr*i)));
                miniatureList.get(i).setY((float) (display.getHeight()/2-210+200*Math.sin(2*Math.PI/nr*i)));
                miniatureList.get(i).savePos();
            }


            //klik na miniature kotwica


            camera.startPreview();
            camera.stopPreview();

            /*SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String d = dFormat.format(new Date());

            File myFoto = new File(dir, "KarolinaFelus");
            myFoto = new File(myFoto, "album1");
            myFoto = new File(myFoto.getAbsolutePath() + "/" + d + ".jpg");
            ///FileOutputStream fs = null;

            try {
                FileOutputStream fs = null;
                fs = new FileOutputStream(myFoto);
                fs.write(fdata);
                fs.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/

            //save(fdata);

            // odswiez kamerę (zapobiega przycięciu się kamery po zrobieniu zdjęcia)
            camera.startPreview();
        }
    };

    private int getCameraId() {
        int cid = 0;
        int camerasCount = Camera.getNumberOfCameras(); // gdy więcej niż jedna kamera
        for (int i = 0; i < camerasCount; i++) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);
            /*if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                cid = i;
            }*/
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cid = i;
            }
        }
        return cid;
    }

    private void initPreview() {
        _cameraPreview = new CameraPreview(CameraActivity.this, camera);
        _frameLayout = (FrameLayout) findViewById(R.id.frameLayout1);
        _frameLayout.addView(_cameraPreview);
    }

    public void showPreview(Mini miniature){
        final ImageView iv = new ImageView(CameraActivity.this);
        byte[] data = takenPhoto.get(miniatureList.indexOf(miniature));
        Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);
        iv.setImageBitmap(b);
        iv.setRotation(-90f);

        final FrameLayout fl = new FrameLayout(CameraActivity.this);
        fl.setBackgroundColor(Color.BLACK);
        fl.addView(iv);
        fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _frameLayout.removeView(fl);
            }
        });

        _frameLayout.addView(fl);
    }

    public void deleteMini(Mini miniature){
        int c = miniatureList.indexOf(miniature);
        _frameLayout.removeView(miniatureList.get(c));
        takenPhoto.remove(c);
        miniatureList.remove(miniature);
    }

    public void deleteAllMini(){
        for(int i = 0, length = miniatureList.size();i<length;i++){
            _frameLayout.removeView(miniatureList.get(i));
        }
        miniatureList.clear();
        takenPhoto.clear();
        //rearrangeMiniatures(); //nie dziala!
    }

    public void saveMini(Mini miniature){
        SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String d = dFormat.format(new Date());

        File myFoto = new File(dir, "KarolinaFelus");
        myFoto = new File(myFoto, "album1");
        //separatorChar to jest taki myk, ze wstawia odpowiedni znak do sciezki, zeby na roznych
        //platformach moglo dzialac, pomysl jarka bonka, bo mondry jest on
        myFoto = new File(myFoto.getAbsolutePath()+ File.separatorChar + d + ".jpg");
        ///FileOutputStream fs = null;

        try {
            FileOutputStream fs = new FileOutputStream(myFoto);
            byte[] data = takenPhoto.get(miniatureList.indexOf(miniature));
            Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);
            Matrix matrix = new Matrix();
            matrix.postRotate(-90f);
            b = Bitmap.createBitmap(b,0,0,b.getWidth(),b.getHeight(),matrix,true);
            b.compress(Bitmap.CompressFormat.JPEG,30,fs);
            fs.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.deleteMini(miniature);
    }

    public void saveAllMini(){
        for(int i = 0, length = takenPhoto.size();i<length;i++){
            SimpleDateFormat dFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String d = dFormat.format(new Date());

            File myFoto = new File(dir, "KarolinaFelus");
            myFoto = new File(myFoto, "album1");
            //separatorChar to jest taki myk, ze wstawia odpowiedni znak do sciezki, zeby na roznych
            //platformach moglo dzialac, pomysl jarka bonka, bo mondry jest on
            myFoto = new File(myFoto.getAbsolutePath()+ File.separatorChar + d + ".jpg");
            ///FileOutputStream fs = null;

            try {
                FileOutputStream fs = new FileOutputStream(myFoto);
                byte[] data = takenPhoto.get(i);
                Bitmap b = BitmapFactory.decodeByteArray(data, 0, data.length);
                Matrix matrix = new Matrix();
                matrix.postRotate(-90f);
                b = Bitmap.createBitmap(b,0,0,b.getWidth(),b.getHeight(),matrix,true);
                b.compress(Bitmap.CompressFormat.JPEG,30,fs);
                fs.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.deleteAllMini();
    }

    public void pokaWaita(){
        TextView wait = (TextView) findViewById(R.id.wait);
        wait.setVisibility(View.VISIBLE);
    }

    public void chowajWaita(){
        TextView wait = (TextView) findViewById(R.id.wait);
        wait.setVisibility(View.GONE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // jeśli nie zwolnimy (release) kamery
        //inna aplikacje nie może jej używać
        if (camera != null) {
            camera.stopPreview();
            //linijka nieudokumentowana w API, bez niej jest crash przy wznawiamiu kamery
            _cameraPreview.getHolder().removeCallback(_cameraPreview);
            camera.release();
            camera = null;
            orientationEventListener.disable();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null) {
            //zainicjalizuj kamerę
            //od nowa
            initCamera();
            initPreview();
            camParams = camera.getParameters();

            colorEffectsList = camParams.getSupportedColorEffects(); // efekty kolorystyczne
            resolutionsList = camParams.getSupportedPictureSizes();
            whiteBalanceList = camParams.getSupportedWhiteBalance();
            minExposureCompensation = camParams.getMinExposureCompensation();
            maxExposureCompensation = camParams.getMaxExposureCompensation();
            currentExposureCompensation = camParams.getExposureCompensation();

            Log.d("lolxd", "Supported Exposure Modes:" + camParams.getMinExposureCompensation());

            Circle circle = new Circle(this);
            _frameLayout.addView(circle);

            circle.setX(display.getWidth()/2-200);
            circle.setY(display.getHeight()/2-300);

            if (orientationEventListener.canDetectOrientation()) {
                Log.wtf("d","Listener dziala");
                orientationEventListener.enable();
            } else {
                Log.wtf("d","Listener nie dziala");
            }
        }
    }
}
