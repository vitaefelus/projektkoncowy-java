package com.projekty.felus.projektkoncowy_felus;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BolsiQ on 2016-11-16.
 */
public class FontsAdapter extends ArrayAdapter {

    private String[] array;
    private Context _context;
    private int _resource;

    public FontsAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        array = objects;
        _context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.font_cell, null);


        //szukam kontrolki w layoucie

        TextView tv1 = (TextView) convertView.findViewById(R.id.tvFontCell);
        tv1.setText(array[position]);
        //
        ImageView iv1 = (ImageView) convertView.findViewById(R.id.ivFontCell);

        if(position == 0){
            iv1.setImageResource(R.drawable.alphabetical);
        }else{
            iv1.setImageResource(R.drawable.folder);
        }

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // klik w obrazek
            }
        });

        return convertView;
    }
}

