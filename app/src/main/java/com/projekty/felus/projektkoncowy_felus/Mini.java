package com.projekty.felus.projektkoncowy_felus;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by BolsiQ on 2016-10-17.
 */
public class Mini extends ImageView {
    int height;
    int width;
    Bitmap bitmap;
    Context context;
    int x,y;

    public Mini(final Context context, int Height, int Width, Bitmap Bitmap) {
        super(context);
        height=Height;
        width=Width;
        bitmap=Bitmap;
        this.context = context;
        ViewGroup.LayoutParams params=new ViewGroup.LayoutParams(width,height);
        Log.wtf("qqqqq", String.valueOf(params));
        this.setLayoutParams(params);
        listeners();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1);
        paint.setColor(Color.CYAN);
        canvas.drawRect(0,0,height+1,width+1,paint);
        if(bitmap != null){
            canvas.drawBitmap(bitmap,1,1,paint);
        }
    }

    public void savePos(){
        this.x= ((int) this.getX());
        this.y= ((int) this.getY());

    }

    private void listeners(){

        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_MOVE:{
                        if(event.getRawX() - Mini.this.getX() > 200){
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((CameraActivity)context).deleteMini(Mini.this);
                                    ((CameraActivity)context).chowajWaita();
                                }
                            }, 1);
                            ((CameraActivity)context).pokaWaita();
                        }
                    }
                }
                return false;
            }
        });

        this.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View arg0) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Opcje");

                final String[] opts = {
                        "Podgląd","usuń bieżące","usuń wszystkie","zapisz bieżące","zapisz wszystkie"
                };

                //"Podgląd","usuń bieżące","usuń wszystkie","zapisz bieżące","zapisz wszystkie"

                alert.setItems(opts, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // wyswietl opcje[which]);
                        Handler handler = new Handler();
                        switch(opts[which]){
                            case "Podgląd":{
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((CameraActivity)context).showPreview(Mini.this);
                                        ((CameraActivity)context).chowajWaita();
                                    }
                                }, 1);
                                ((CameraActivity)context).pokaWaita();
                                break;
                            }
                            case "usuń bieżące":{
                                Log.wtf("wybrana opcja:","Usuń bieżące");
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((CameraActivity)context).deleteMini(Mini.this);
                                        ((CameraActivity)context).chowajWaita();
                                    }
                                }, 1);

                                ((CameraActivity)context).pokaWaita();
                                break;
                            }
                            case "usuń wszystkie":{
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((CameraActivity)context).deleteAllMini();
                                        ((CameraActivity)context).chowajWaita();
                                    }
                                }, 1);
                                ((CameraActivity)context).pokaWaita();
                                break;
                            }

                            case "zapisz bieżące":{
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((CameraActivity)context).saveMini(Mini.this);
                                        ((CameraActivity)context).chowajWaita();
                                    }
                                }, 1);
                                ((CameraActivity)context).pokaWaita();
                                break;
                            }

                            case "zapisz wszystkie":{
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        ((CameraActivity)context).saveAllMini();
                                        ((CameraActivity)context).chowajWaita();
                                    }
                                }, 1);
                                ((CameraActivity)context).pokaWaita();
                                break;
                            }

                        }
                    }
                });
                alert.show();

                return true;
            }
        });

    }

/*    protected void deleteAll(Canvas canvas){
        canvas.clear();
    }*/
}

