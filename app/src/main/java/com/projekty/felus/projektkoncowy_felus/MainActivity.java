package com.projekty.felus.projektkoncowy_felus;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private LinearLayout cameraClick;
    private LinearLayout albumsClick;
    private LinearLayout collagueClick;
    private LinearLayout networkClick;
    private ImageView arrowRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //system plikow

        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if(!file.exists()){
            file.mkdir();
        }
        File dir = new File(file,"KarolinaFelus");
        if(!dir.exists()){
            dir.mkdir();

            File[] albums = new File[3];
            for(int i = 0; i <3 ; i++){
                albums[i] = new File(dir,"album"+i);
                albums[i].mkdir();
            }

        }

        cameraClick = (LinearLayout) findViewById(R.id.cameraClick);
        cameraClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,CameraActivity.class);
                startActivity(intent);
                /*Toast.makeText(MainActivity.this,
                        "WHQADHAW",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        arrowRight = (ImageView) findViewById(R.id.arrowRight);
        arrowRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentF = new Intent(MainActivity.this,FotoActivity.class);
                startActivity(intentF);
            }
        });

        albumsClick = (LinearLayout) findViewById(R.id.albumsClick);
        albumsClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(MainActivity.this,AlbumsActivity.class);
                startActivity(intent1);
                /*Toast.makeText(MainActivity.this,
                        "Albumy",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        collagueClick = (LinearLayout) findViewById(R.id.collagueClick);
        collagueClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(MainActivity.this,CollagueActivity.class);
                startActivity(intent2);
                /*Toast.makeText(MainActivity.this,
                        "Kolaż",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        networkClick = (LinearLayout) findViewById(R.id.networkClick);
        networkClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MainActivity.this,NetworkActivity.class);
                startActivity(intent3);
                /*Toast.makeText(MainActivity.this,
                        "Zobacz w sieci",
                        Toast.LENGTH_SHORT).show();*/
            }
        });
    }


}
