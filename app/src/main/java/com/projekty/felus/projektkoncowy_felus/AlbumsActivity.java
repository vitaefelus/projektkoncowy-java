package com.projekty.felus.projektkoncowy_felus;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class AlbumsActivity extends AppCompatActivity {

    File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"KarolinaFelus");
    File[] al = dir.listFiles();
    String[] albumsArrList = new String[al.length];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albums);
        this.restartView();
    }

    public void restartView(){
        //get GridView from XML
        GridView gridView = (GridView)findViewById(R.id.gridView);

        //store subdirectories in ArrayList


        for (int i = 0; i < al.length; i++){
            if (al[i].isDirectory())
                albumsArrList[i] = al[i].getName();
            // Log.d("FILE", f.getName());
        }

        //initialize array adapter for GridView
        MyAA myaa = new MyAA(
                AlbumsActivity.this,     // Context
                R.layout.land_cell,     // nazwa pliku xml naszej komórki
                albumsArrList );         // tablica przechowująca dane

        //append array adapter to view
        gridView.setAdapter(myaa);

        //handle click event on gridview cell
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //test
                Log.d("TAG","index = " + i);
                al = al[i].listFiles();
                Arrays.sort(al);
                albumsArrList = new String[al.length];
                for (int j = 0; j < al.length; j++) {
                    albumsArrList[j] = al[j].getName();
                }

                Intent albumsIntent = new Intent(AlbumsActivity.this,ImagesActivity.class);
                albumsIntent.putExtra("path", al[i].getParentFile().getPath());
                startActivity(albumsIntent);
            }
        });

    }
}
