package com.projekty.felus.projektkoncowy_felus;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.io.File;

public class ImagesActivity extends Activity {

    File[] files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);


        Bundle bundle = getIntent().getExtras();
        String parentName = bundle.getString("path");

        File parentFolder = new File(parentName);
        files = parentFolder.listFiles();
        Log.d("KF", parentFolder.getPath());

        LinearLayout parentLayout = (LinearLayout) findViewById(R.id.linear0);

        ViewGroup.LayoutParams lparams;
        int odd = 1;

        for (int i = 0; i < files.length; i += 2) {
            Log.d("KF", files[i].getName());
            LinearLayout LL = new LinearLayout(this);
            LL.setBackgroundColor(Color.WHITE);
            LL.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LL.setLayoutParams(LLParams);
            parentLayout.addView(LL);

            if (odd == 2) {
                lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2);
                odd = 1;
            }
            else {
                lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                odd = 2;
            }

            if (files[i].isFile()) {
                ImageView imgv01 = new ImageView(this);
                String imagepath = files[i].getPath();
                Bitmap bmp = betterImageDecode(imagepath);
                LL.addView(imgv01);
                imgv01.setImageBitmap(bmp);
                imgv01.setLayoutParams(lparams);
                imgv01.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            if (odd == 2) {
                lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 2);
            }
            else {
                lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            }
            if(i + 1 <= files.length)
                if (files[i + 1].isFile()) {
                    ImageView imgv01 = new ImageView(this);
                    String imagepath = files[i + 1].getPath();
                    Bitmap bmp = betterImageDecode(imagepath);
                    LL.addView(imgv01);
                    imgv01.setImageBitmap(bmp);
                    imgv01.setLayoutParams(lparams);
                    imgv01.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
        }
    }

    private Bitmap betterImageDecode(String filePath) {

        Bitmap myBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();    //opcje przekształcania bitmapy
        options.inSampleSize = 4; // zmniejszenie jakości bitmapy 4x
        //
        myBitmap = BitmapFactory.decodeFile(filePath, options);
        return myBitmap;
    }
}

