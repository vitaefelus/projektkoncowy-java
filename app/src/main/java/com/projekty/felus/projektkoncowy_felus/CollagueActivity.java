package com.projekty.felus.projektkoncowy_felus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class CollagueActivity extends AppCompatActivity {

    private ArrayList<ImageData> list = new ArrayList<>();

    private LinearLayout twoElements;
    private LinearLayout threeElements;
    private LinearLayout thisOneIsComplicated;
    private Display display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collague);
        getSupportActionBar().hide();

        twoElements = (LinearLayout) findViewById(R.id.twoElements);
        threeElements = (LinearLayout) findViewById(R.id.threeElements);
        thisOneIsComplicated = (LinearLayout) findViewById(R.id.thisOneIsComplicated);

        display = getWindowManager().getDefaultDisplay();

        initListeners();
    }

    private void initListeners(){
        twoElements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list.add(new ImageData(
                        0,
                        0,
                        display.getWidth(),
                        display.getHeight()/2));

                list.add(new ImageData(
                        0,
                        display.getHeight()/2+1,
                        display.getWidth(),
                        display.getHeight()/2));

                Intent intent = new Intent(CollagueActivity.this,PickedCollagueActivity.class);
                intent.putExtra("list", list);
                startActivity(intent);

            }
        });

        threeElements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                list.add(new ImageData(
                        0,
                        0,
                        display.getWidth()/2,
                        display.getHeight()));

                list.add(new ImageData(
                        display.getWidth()/2+1,
                        0,
                        display.getWidth()/2,
                        display.getHeight()/2));

                list.add(new ImageData(
                        display.getWidth()/2+1,
                        display.getHeight()/2+1,
                        display.getWidth()/2,
                        display.getHeight()/2));

                Intent intent = new Intent(CollagueActivity.this,PickedCollagueActivity.class);
                intent.putExtra("list", list);
                startActivity(intent);

            }
        });

        thisOneIsComplicated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.clear();
                //1
                list.add(new ImageData(
                        0,
                        0,
                        2*display.getWidth()/3,
                        display.getHeight()/3));
                //2
                list.add(new ImageData(
                        2*display.getWidth()/3+1,
                        0,
                        display.getWidth()/3,
                        display.getHeight()/3));
                //3
                list.add(new ImageData(
                        0,
                        display.getHeight()/3+1,
                        display.getWidth()/3,
                        display.getHeight()/3));
                //4
                list.add(new ImageData(
                        display.getWidth()/3+1,
                        (display.getHeight()/3)+1, //+1
                        2*display.getWidth()/3,
                        display.getHeight()/3));
                //5
                list.add(new ImageData(
                        0,
                        2*display.getHeight()/3+2,
                        2*display.getWidth()/3,
                        display.getHeight()/3));
                //6
                list.add(new ImageData(
                        2*display.getWidth()/3+1,
                        2*display.getHeight()/3+2,
                        display.getWidth()/3,
                        display.getHeight()/3));


                Intent intent = new Intent(CollagueActivity.this,PickedCollagueActivity.class);
                intent.putExtra("list", list);
                startActivity(intent);

            }
        });
    }


}
