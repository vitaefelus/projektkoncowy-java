package com.projekty.felus.projektkoncowy_felus;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class FotoActivity extends AppCompatActivity {

    FontsAdapter fAdapter;
    ImageView abc;
    DrawerLayout drawerLayout;
    Display display;
    private float width, height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);
        getSupportActionBar().hide();

        abc = (ImageView) findViewById(R.id.abc);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        abc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        final String[] array = {
                "fonts",
                "jakieś",
                "kurczaczki",
                "opcje na kiju"
        };

        ListView listView = (ListView) findViewById(R.id.lV);

        FontsAdapter fAdapter = new FontsAdapter(
                FotoActivity.this,
                R.layout.font_cell,
                array
        );

        listView.setAdapter(fAdapter);

        display = ((Activity)this).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;


        listView.getLayoutParams().width = (int) (2*width/3);
        listView.getLayoutParams().height = (int) height;

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    //Log.wtf("KF","XDDDDDD");
                    Intent intent = new Intent(FotoActivity.this,LetterActivity.class);
                    startActivityForResult(intent,1);
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            Log.wtf("kornel","pomocny jest");

        }
    }
}
