package com.projekty.felus.projektkoncowy_felus;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;

public class LetterActivity extends AppCompatActivity {

    ImageView letterAccept;
    String[] fontArray;
    LinearLayout lL;
    Display display;
    EditText editText;
    ScrollView scrollViewFonts;
   // PreviewText previewText;

    private float width, height, eHeight;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letter);

        letterAccept = (ImageView) findViewById(R.id.letterAccept);
        lL = (LinearLayout) findViewById(R.id.lL);
        editText = (EditText) findViewById(R.id.editText);
        scrollViewFonts = (ScrollView) findViewById(R.id.scrollViewFonts);

        editText.setBackgroundColor(Color.WHITE);

        display = ((Activity)this).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        AssetManager assetManager = getAssets();
        try {
            fontArray = assetManager.list("fonts"); // fonts to nazwa podfolderu w assets

            for(int j=0, length = fontArray.length;j<length;j++){
                TextView textView = new TextView(this);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 150);
                textView.setLayoutParams(params);

                final Typeface tf=Typeface.createFromAsset(getAssets(),"fonts/" + fontArray[j]);
                textView.setTypeface (tf);

                textView.setText("FONT FONT FONT FONT");
                textView.setTextColor(Color.BLACK);
                textView.setTextSize(35);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editText.setTypeface(tf);
                        editText.setTextSize(40);
                    }
                });

                lL.addView(textView);
               // PreviewText previewText = new PreviewText(LetterActivity.this,tf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        TextWatcher textWatcher = new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               // previewLayout.removeAllViews();
               // previewLayout.addView (previewText)
            }
        };


        letterAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
    }
}
