package com.projekty.felus.projektkoncowy_felus;

import java.io.Serializable;

/**
 * Created by BolsiQ on 2016-11-08.
 */
public class ImageData implements Serializable{
    @Override
    public String toString() {
        return "ImageData{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    private float x,y,width,height;

    public ImageData(float x, float y, float width, float height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

}
